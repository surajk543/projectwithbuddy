package com.assignment.projectwithbuddy.cron;

import com.assignment.projectwithbuddy.model.Transactions;
import com.assignment.projectwithbuddy.repository.TransactionsRepository;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Component
public class ReportCron {
    @Autowired private TransactionsRepository transactionsRepository;
    @Scheduled(fixedRate = 60*60*1000)
    public void generateReport() throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
        List<Transactions> transactions = transactionsRepository.findAll();
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        String formatDateTime = now.format(format);
        String fileName = "report-"+formatDateTime+".csv";
        Writer writer = Files.newBufferedWriter(Paths.get(fileName));

        StatefulBeanToCsv<Transactions> beanToCsv = new StatefulBeanToCsvBuilder<Transactions>(writer)
                .build();
        beanToCsv.write(transactions);
        writer.close();
    }
}
