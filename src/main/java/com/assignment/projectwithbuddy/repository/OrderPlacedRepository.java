package com.assignment.projectwithbuddy.repository;

import com.assignment.projectwithbuddy.model.OrderPlaced;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.domain.Pageable;
public interface OrderPlacedRepository extends JpaRepository<OrderPlaced,Integer> {
    Page<OrderPlaced> findByUserId(Integer userId,Pageable pageable);
    Page<OrderPlaced> findAll(Pageable pageable);
}
