package com.assignment.projectwithbuddy.repository;

import com.assignment.projectwithbuddy.model.MostOrderedProducts;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MostOrderedProductsRepository extends JpaRepository<MostOrderedProducts,Integer> {
    MostOrderedProducts findByProductId(Integer productId);
}
