package com.assignment.projectwithbuddy.repository;

import com.assignment.projectwithbuddy.model.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserAccountRepository extends JpaRepository<UserAccount,Integer> {
}
