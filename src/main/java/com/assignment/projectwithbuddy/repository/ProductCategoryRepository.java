package com.assignment.projectwithbuddy.repository;

import com.assignment.projectwithbuddy.model.ProductCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductCategoryRepository extends JpaRepository<ProductCategory,Integer> {

}
