package com.assignment.projectwithbuddy.repository;

import com.assignment.projectwithbuddy.model.Transactions;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionsRepository extends JpaRepository<Transactions,Integer> {
}
