package com.assignment.projectwithbuddy.repository;

import com.assignment.projectwithbuddy.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import org.springframework.data.domain.Pageable;
import java.util.List;

public interface ProductRepository extends JpaRepository<Product,Integer>, PagingAndSortingRepository<Product,Integer> {
    @Query("select u from Product u where u.productName like %:productName% and u.categoryId = :categoryId")
    Page<Product> findByCategoryIdAndProductName(@Param("categoryId") int categoryId,
                                   @Param("productName") String productName,Pageable pageable);
    Page<Product> findByCategoryId(@Param("categoryId") int categoryId,Pageable pageable);
    Page<Product> findByProductName(@Param("productName") String productName,Pageable pageable);
    Page<Product> findAllByProductName(String productName,Pageable pageable);
    Page<Product> findAll(Pageable pageable);
    Page<Product> findByUserId(Integer userId,Pageable pageable);
}
