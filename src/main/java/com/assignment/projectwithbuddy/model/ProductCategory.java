package com.assignment.projectwithbuddy.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "suraj_kum_productcategory")
public class ProductCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;
    String categoryName;
    @Column(name = "creationDate", nullable = false, updatable = false, insertable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    String creationDate;
}
