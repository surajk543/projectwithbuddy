package com.assignment.projectwithbuddy.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "suraj_kum_orders")
public class OrderPlaced {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Integer id;
    Integer productId;
    Integer userId;
    Integer quantity;
    String deliveryLocation;
    Integer totalCost;
    @Column(name = "creationDate", nullable = false, updatable = false, insertable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    String creationDate;

    public int getUserId() {
        return userId;
    }

    public int getProductId() {
        return productId;
    }

    public int getQuantity() {
        return quantity;
    }
}
