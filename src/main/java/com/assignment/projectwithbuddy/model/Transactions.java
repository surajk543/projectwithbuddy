package com.assignment.projectwithbuddy.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "suraj_kum_transactions")
public class Transactions {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Integer id;
    Integer userId;
    Integer moneyEarn;
    Integer moneySpent;
    Integer productId;
    String paymentBy;
    @Column(name = "creationDate", nullable = false, updatable = false, insertable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    String creationDate;
}
