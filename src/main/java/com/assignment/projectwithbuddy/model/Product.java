package com.assignment.projectwithbuddy.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "suraj_kum_product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Integer id;
    String productName;
    Integer categoryId;
    Integer price;
    Integer quantity;
    Integer userId;
    @Column(name = "creationDate", nullable = false, updatable = false, insertable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    String creationDate;

    public int getCategoryId() {
        return categoryId;
    }
}
