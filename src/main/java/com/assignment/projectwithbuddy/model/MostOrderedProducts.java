package com.assignment.projectwithbuddy.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
public class MostOrderedProducts {
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    Integer id;
    Integer productId;
    Integer items;
}
