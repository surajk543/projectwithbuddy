package com.assignment.projectwithbuddy.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "suraj_kum_useraccount")
public class UserAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Integer id;
    String name;
    String email;
    Integer role;//0 is for admin, 1 is for vendor ,2 is for customer
    String encryptedPassword;
    @Column(name = "creationDate", nullable = false, updatable = false, insertable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    String creationDate;
}
