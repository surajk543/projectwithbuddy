package com.assignment.projectwithbuddy.controller;

import com.assignment.projectwithbuddy.model.UserAccount;
import com.assignment.projectwithbuddy.repository.UserAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
public class UserAccountController {
    @Autowired UserAccountRepository userAccountRepository;
    @PostMapping("/api/v1/user-register")
    public UserAccount insert(@RequestBody UserAccount userAccount){
        if(userAccount.getRole()<0 || userAccount.getRole()>2){
            throw new RuntimeException("Check Your Role");
        }
        return userAccountRepository.save(userAccount);
    }
}
