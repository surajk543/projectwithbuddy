package com.assignment.projectwithbuddy.controller;

import com.assignment.projectwithbuddy.model.ProductCategory;
import com.assignment.projectwithbuddy.repository.ProductCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
public class ProductCategoryController {
    @Autowired
    private ProductCategoryRepository productCategoryRepository;
    @PostMapping("/api/v1/product-categories")
    public ProductCategory insertCategory(@RequestBody ProductCategory productCategory){
        return productCategoryRepository.save(productCategory);
    }
    @GetMapping("/api/v1/product-categories")
    public List<ProductCategory> getall(){
        //LocalDate date = LocalDate.now();
        //System.out.println(date);
        //System.out.println(date.getMonth());
        return productCategoryRepository.findAll();
    }

    @GetMapping("/api/v1/product-categories/{productCategoryId}")
    //Fetch productcategory by id
    public Optional<ProductCategory> getbycategory(@PathVariable int productCategoryId){
        Optional<ProductCategory> optionalProductCategory= productCategoryRepository.findById(productCategoryId);
        if(optionalProductCategory.isPresent()){
            return optionalProductCategory;
        }
        throw new RuntimeException("Not Found");
    }

    @PutMapping("/api/v1/product-categories/{productCategoryId}")
    public ProductCategory updateCategoryName(@PathVariable int productCategoryId,
                                              @RequestBody ProductCategory productCategory){
        Optional<ProductCategory> optionalProductCategory = productCategoryRepository.findById(productCategoryId);
        if(optionalProductCategory.isPresent()){
            //optionalProductCategory.setCategoryName(productCategory.getCategoryName());
            //return  productCategoryRepository.save(optionalProductCategory);
            optionalProductCategory.get().setCategoryName(productCategory.getCategoryName());
        }
        throw new RuntimeException("Updation Failed");
    }

    @DeleteMapping("/api/v1/product-categories/{productCategoryId}")
    public String deleteCategory(@PathVariable int productCategoryId){
        Optional<ProductCategory> optionalProductCategory = productCategoryRepository.findById(productCategoryId);
        if(optionalProductCategory.isPresent()){
            productCategoryRepository.deleteById(productCategoryId);
            return "Deleted Succesfull";
        }
        return "Deleted Unsuccessful";
    }
}
