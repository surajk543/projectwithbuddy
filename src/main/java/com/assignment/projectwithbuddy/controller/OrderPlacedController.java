package com.assignment.projectwithbuddy.controller;

import com.assignment.projectwithbuddy.model.*;
import com.assignment.projectwithbuddy.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
@RestController
@CrossOrigin
public class OrderPlacedController {
    @Autowired UserAccountRepository userAccountRepository;
    @Autowired OrderPlacedRepository orderPlacedRepository;
    @Autowired ProductRepository productRepository;
    @Autowired TransactionsRepository transactionsRepository;
    @Autowired MostOrderedProductsRepository mostOrderedProductsRepository;
    @PostMapping("/api/v1/orders")
    public OrderPlaced insert(@RequestBody OrderPlaced orderPlaced){
        int userId=orderPlaced.getUserId();
        int productId=orderPlaced.getProductId();
        Optional<UserAccount> optionalUserAccount=userAccountRepository.findById(userId);
        Optional<Product> optionalProduct = productRepository.findById(productId);
        return optionalUserAccount.map((function)->{
            return optionalProduct.map(function2->{
                Integer tot_money=orderPlaced.getQuantity()*optionalProduct.get().getPrice();
                orderPlaced.setTotalCost(tot_money);
                Integer vendorId=optionalProduct.get().getUserId();
                // for customers
                Transactions transactions = new Transactions();
                transactions.setMoneyEarn(0);
                transactions.setMoneySpent(tot_money);
                transactions.setUserId(userId);
                transactions.setPaymentBy("Cash");
                transactions.setProductId(productId);
                transactionsRepository.save(transactions);
                //for vendors
                Transactions transactions2 = new Transactions();
                transactions2.setPaymentBy("Cash");
                transactions2.setMoneySpent(0);
                transactions2.setMoneyEarn(tot_money);
                transactions2.setUserId(optionalProduct.get().getUserId());
                transactions2.setProductId(productId);
                transactionsRepository.save(transactions2);
                //increase counts of purchased product
                Optional<MostOrderedProducts> optionalMostOrderedProducts= Optional.
                        ofNullable(mostOrderedProductsRepository.
                        findByProductId(productId));
                if(optionalMostOrderedProducts.isPresent()){
                    optionalMostOrderedProducts.get().setItems(1+optionalMostOrderedProducts.get().getItems());
                    mostOrderedProductsRepository.save(optionalMostOrderedProducts.get());
                }else{
                    MostOrderedProducts mostOrderedProducts = new MostOrderedProducts();
                    mostOrderedProducts.setProductId(productId);
                    mostOrderedProducts.setItems(1);
                    mostOrderedProductsRepository.save(mostOrderedProducts);
                }
                //
                return orderPlacedRepository.save(orderPlaced);
            })
                    .orElseThrow(()->{throw new RuntimeException("Product Does not exist");});
            //return orderPlacedRepository.save(orderPlaced);
        }).orElseThrow(()->{throw  new RuntimeException("User Does not exist");});
    }

    @GetMapping("/api/v1/orders")
    public List<OrderPlaced> getall(@RequestParam(required = false) Integer userId,
                                    @RequestParam(required = false) Integer pageNumber,
                                    @RequestParam(required = false) Integer pageSize){
        if(pageNumber==null){pageNumber=0;}
        if(pageSize==null){pageSize=10;}
        if(userId!=null){
            return orderPlacedRepository.findByUserId(userId,
                    (Pageable) PageRequest.of(pageNumber,pageSize)).getContent();
        }
        return orderPlacedRepository.findAll((Pageable)PageRequest.of(pageNumber,pageSize)).getContent();
    }
}
