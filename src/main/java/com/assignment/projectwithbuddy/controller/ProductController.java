package com.assignment.projectwithbuddy.controller;

import com.assignment.projectwithbuddy.model.Product;
import com.assignment.projectwithbuddy.model.ProductCategory;
import com.assignment.projectwithbuddy.model.UserAccount;
import com.assignment.projectwithbuddy.repository.ProductCategoryRepository;
import com.assignment.projectwithbuddy.repository.ProductRepository;
import com.assignment.projectwithbuddy.repository.UserAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;
@CrossOrigin
@RestController
public class ProductController {
    @Autowired ProductRepository productRepository;
    @Autowired ProductCategoryRepository productCategoryRepository;
    @Autowired UserAccountRepository userAccountRepository;

    @GetMapping("/api/v1/products")
    public List<Product> getProducts(@RequestParam(required = false)Integer categoryId,
                                     @RequestParam(required = false)String productName,
                                     @RequestParam(required = false)Integer pageNumber,
                                     @RequestParam(required = false)Integer userId,
                                     @RequestParam(required = false)Integer pageSize){
        //Pageable firstPageWithTwoElements = (Pageable) PageRequest.of(0, 2);
        //return productRepository.findAll(firstPageWithTwoElements).getContent();
        if(pageNumber==null){
            pageNumber=0;//by default
        }
        if(pageSize==null){
            pageSize=10;//by default
        }
        if(categoryId==null && productName==null && userId!=null){
            return productRepository.findByUserId(userId,(Pageable) PageRequest.of(pageNumber,pageSize)).getContent();
        }
        else if(categoryId!=null && productName==null && userId==null){
            return productRepository.findByCategoryId(categoryId,
                    (Pageable) PageRequest.of(pageNumber,pageSize)).getContent();
        }
        else if(categoryId==null && productName!=null && userId==null){
            return productRepository.findByProductName(productName,
                    (Pageable) PageRequest.of(pageNumber,pageSize)).getContent();
        }
        else  if(categoryId!=null && productName!=null && userId==null) {
            return productRepository.findByCategoryIdAndProductName(categoryId, productName,
                    (Pageable) PageRequest.of(pageNumber, pageSize)).getContent();
        }
        return productRepository.findAll((Pageable) PageRequest.of(pageNumber,pageSize)).getContent();
    }
    @PostMapping("/api/v1/products")
    public Product insert(@RequestBody Product newProduct){
        int category_id=newProduct.getCategoryId();
        if(newProduct.getUserId()==null){
            throw new RuntimeException("UserId not provided");
        }
        Optional<UserAccount> optionalUserAccount = userAccountRepository.findById(newProduct.getUserId());
        if(optionalUserAccount.isPresent()){}else{throw new RuntimeException("UserId not registered");}
        Optional<ProductCategory> optionalProductCategory = productCategoryRepository.findById(category_id);
        return optionalProductCategory.map(productCategory -> productRepository.save(newProduct))
                .orElseThrow(()->{throw  new RuntimeException("Product Category is not Valid");});
    }

    @PutMapping("/api/v1/products")
    public Product updateproduct(@RequestParam(required = true)Integer productId,
                                 @RequestBody Product newProduct){
        Optional<Product> oldProduct = productRepository.findById(productId);
        if(oldProduct.isPresent()){
            if(newProduct.getProductName()!=null){
                oldProduct.get().setProductName(newProduct.getProductName());
            }
            if(newProduct.getPrice()!=null){
                oldProduct.get().setPrice(newProduct.getPrice());
            }
            if(newProduct.getQuantity()!=null){
                oldProduct.get().setQuantity(newProduct.getQuantity());
            }
            return productRepository.save(oldProduct.get());
        }
        throw new RuntimeException("Updation Failed");
    }

    @DeleteMapping("/api/v1/products/{productId}")
    public String deleteProduct(@PathVariable Integer productId){
        Optional<Product> optionalProduct = productRepository.findById(productId);
        if(optionalProduct.isPresent()){
            productRepository.deleteById(productId);
            return "Delete Successfull";
        }
        return "Delete UnSuccessfull";
    }

}
